// SPDX-License-Identifier: GPL-2.0+
/*
 * (C) Copyright 2015 Rockchip Electronics Co., Ltd
 */

#include <common.h>
#include <dm.h>
#include <asm/io.h>
#include <asm/arch-rockchip/uart.h>
#include <asm/arch-rockchip/sdram_rk3036.h>

void get_ddr_config(struct rk3036_ddr_config *config)
{
	/* K4T1G164QE HCE6 config */
	config->ddr_type = 2;
	config->rank = 2;
	config->cs0_row = 13;
	config->cs1_row = 13;

	/* 8bank */
	config->bank = 3;
	config->col = 10;

	/* 16bit bw */
	config->bw = 1;
}
